## Nostra Backend SPringBoot

## Running My Apps
```
java -jar project

```

## Running My Server
```
./mvn spring-boot:run
```

## Documentation Swagger UI
``` 
Documentation Swagger open link http://localhost:8080/swageer-ui.html
```


## setup configuration username and password
```spring.jpa.show-sql=true
spring.h2.console.enabled=true
spring.datasource.url=jdbc:h2:~/te
spring.datasource.username=
spring.datasource.password=
spring.datasource.driver-class-name=org.h2.Driver
spring.application.name=NostraMovieApp
server.port=8080
```